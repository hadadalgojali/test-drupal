<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/td_banner/templates/banner.html.twig */
class __TwigTemplate_23a90b4b6cb7a938dc2489c151bcb5f04de0444159501f26e395a03b0875a3ea extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 1, "for" => 3];
        $filters = ["escape" => 4];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'for'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if ( !twig_test_empty(($context["data"] ?? null))) {
            // line 2
            echo "
    ";
            // line 3
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["data"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
                // line 4
                echo "        <header class=\"masthead text-white text-center\" style=\"background: url('../../../sites/default/files/2020-03/";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($context["banner"], "field_banner_image", []), "entity", []), "getFilename", [], "method")), "html", null, true);
                echo "') no-repeat center center;\">
            <div class=\"overlay\"></div>
            <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-xl-9 mx-auto\">
                <h1 class=\"mb-5\">";
                // line 9
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["banner"], "field_banner_title", []), "value", [])), "html", null, true);
                echo "</h1>
                ";
                // line 10
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["banner"], "field_banner_description", []), "value", [])), "html", null, true);
                echo "
                </div>
                <div class=\"col-md-10 col-lg-8 col-xl-7 mx-auto\">
                <form>
                    <div class=\"form-row\">
                    <div class=\"col-12 col-md-9 mb-2 mb-md-0\">
                        <input type=\"email\" class=\"form-control form-control-lg\" placeholder=\"Enter your email...\">
                    </div>
                    <div class=\"col-12 col-md-3\">
                        <button type=\"submit\" class=\"btn btn-block btn-lg btn-primary\">Sign up!</button>
                    </div>
                    </div>
                </form>
                </div>
            </div>
            </div>
        </header>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
        }
    }

    public function getTemplateName()
    {
        return "modules/custom/td_banner/templates/banner.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 10,  73 => 9,  64 => 4,  60 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/td_banner/templates/banner.html.twig", "C:\\laragon\\www\\test_drupal\\web\\modules\\custom\\td_banner\\templates\\banner.html.twig");
    }
}
