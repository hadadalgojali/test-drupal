<?php

namespace Drupal\td_news\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Create News Block
 * 
 * @Block(
 *  id = "block_news",
 *  admin_label = @Translation("News test drupal")
 * )
 */
class News extends BlockBase
{
    /**
     * {@inheritdoc}
     */
    public function build()
    {
        // remove cache for anonymous request
        // $this->getCacheMaxAge();
        return [
            '#theme' => 'news',
            '#data' => $this->getData(),
            '#cache' => [
                'max-age' => 2
              ]
        ];
    }

    public function getCacheMaxAge() {
        \Drupal::service('page_cache_kill_switch')->trigger();
    
        return 0;
    }


    private function getData()
	{
        $query = \Drupal::entityQuery('node')
            ->condition('type', 'news') //content type
            ->execute();

        $node = node_load_multiple($query);

        return $node;
	}
}