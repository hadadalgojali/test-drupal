<?php

namespace Drupal\td_event\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Create Event Block
 * 
 * @Block(
 *  id = "block_event",
 *  admin_label = @Translation("Event test drupal")
 * )
 */
class Event extends BlockBase
{
    /**
     * {@inheritdoc}
     */
    public function build()
    {
        // remove cache for anonymous request
        // $this->getCacheMaxAge();
        return [
            '#theme' => 'event',
            '#data' => $this->getData(),
            '#cache' => [
                'max-age' => 2
              ]
        ];
    }

    public function getCacheMaxAge() {
        \Drupal::service('page_cache_kill_switch')->trigger();
    
        return 0;
    }


    private function getData()
	{
        $query = \Drupal::entityQuery('node')
            ->condition('type', 'event') //content type
            ->execute();

        $node = node_load_multiple($query);

        return $node;
	}
}