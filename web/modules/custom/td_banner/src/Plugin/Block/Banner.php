<?php

namespace Drupal\td_banner\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Create Banner Block
 * 
 * @Block(
 *  id = "block_banner",
 *  admin_label = @Translation("Banner test drupal")
 * )
 */
class Banner extends BlockBase
{
    /**
     * {@inheritdoc}
     */
    public function build()
    {
        // remove cache for anonymous request
        // $this->getCacheMaxAge();
        // var_dump($this->getData());die;
        return [
            '#theme' => 'banner',
            '#data' => $this->getData(),
            '#cache' => [
                'max-age' => 2
              ]
        ];
    }
    public function getCacheMaxAge() {
        \Drupal::service('page_cache_kill_switch')->trigger();
    
        return 0;
    }


    private function getData()
	{
        $query = \Drupal::entityQuery('node')
            ->condition('type', 'banner') //content type
            ->execute();

        $node = node_load_multiple($query);

        return $node;
	}
}